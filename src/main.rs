#![feature(duration_as_u128, test)]

extern crate sdl2;
extern crate test;

use sdl2::image::LoadTexture;
use sdl2::render::{Canvas, Texture, TextureCreator};
use sdl2::video::{Window, WindowContext};
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::event::Event;
use sdl2::keyboard::Scancode;
use std::collections::HashMap;
use std::time::SystemTime;
use std::mem;
//use std::cmp;

enum SpriteDuration {
    Ms(u32),
    Step(u32),
    Infinite,
}

trait Sprite {
    fn next(&self) -> &'static Sprite;
    fn duration(&self) -> &'static SpriteDuration;
    fn path(&self) -> &'static str;
}

struct Idle1;
struct Idle2;
struct Idle3;
struct Idle4;

impl Sprite for Idle1 {
    fn next(&self) -> &'static Sprite {
        &Idle2
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Ms(175)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-idle-00.png"
    }
}

impl Sprite for Idle2 {
    fn next(&self) -> &'static Sprite {
        &Idle3
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Ms(175)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-idle-01.png"
    }
}

impl Sprite for Idle3 {
    fn next(&self) -> &'static Sprite {
        &Idle4
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Ms(175)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-idle-02.png"
    }
}

impl Sprite for Idle4 {
    fn next(&self) -> &'static Sprite {
        &Idle1
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Ms(175)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-idle-03.png"
    }
}

struct Running1;
struct Running2;
struct Running3;
struct Running4;
struct Running5;
struct Running6;

impl Sprite for Running1 {
    fn next(&self) -> &'static Sprite {
        &Running2
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Step(8)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-run-00.png"
    }
}

impl Sprite for Running2 {
    fn next(&self) -> &'static Sprite {
        &Running3
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Step(8)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-run-01.png"
    }
}

impl Sprite for Running3 {
    fn next(&self) -> &'static Sprite {
        &Running4
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Step(8)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-run-02.png"
    }
}

impl Sprite for Running4 {
    fn next(&self) -> &'static Sprite {
        &Running5
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Step(8)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-run-03.png"
    }
}

impl Sprite for Running5 {
    fn next(&self) -> &'static Sprite {
        &Running6
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Step(8)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-run-04.png"
    }
}

impl Sprite for Running6 {
    fn next(&self) -> &'static Sprite {
        &Running1
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Step(8)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-run-05.png"
    }
}

struct PrepareJump1;
struct PrepareJump2;
struct PrepareJump3;
struct PrepareJump4;

impl Sprite for PrepareJump1 {
    fn next(&self) -> &'static Sprite {
        &PrepareJump2
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Ms(80)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-jump-00.png"
    }
}

impl Sprite for PrepareJump2 {
    fn next(&self) -> &'static Sprite {
        &PrepareJump3
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Ms(80)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-jump-01.png"
    }
}

impl Sprite for PrepareJump3 {
    fn next(&self) -> &'static Sprite {
        &PrepareJump4
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Ms(80)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-jump-02.png"
    }
}

impl Sprite for PrepareJump4 {
    fn next(&self) -> &'static Sprite {
        &PrepareJump1
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Infinite
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-jump-03.png"
    }
}

struct Jumping1;

impl Sprite for Jumping1 {
    fn next(&self) -> &'static Sprite {
        &Jumping1
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Infinite
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-jump-03.png"
    }
}

struct Falling1;
struct Falling2;

impl Sprite for Falling1 {
    fn next(&self) -> &'static Sprite {
        &Falling2
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Step(8)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-fall-00.png"
    }
}

impl Sprite for Falling2 {
    fn next(&self) -> &'static Sprite {
        &Falling1
    }

    fn duration(&self) -> &'static SpriteDuration {
        &SpriteDuration::Step(8)
    }

    fn path(&self) -> &'static str {
        "assets/adventurer-fall-01.png"
    }
}

struct PlayerTexture {
    normal: Texture,
    flipped: Texture,
}

struct Animation {
    sprite: &'static Sprite,
    textures: HashMap<&'static str, PlayerTexture>,
    timestamp: Option<SystemTime>,
    movement_step: u32,
    tick: u32,
}

#[derive(PartialEq)]
enum State {
    Idle,
    Running,
    PrepareJump,
    Jumping,
}

#[derive(PartialEq)]
enum Direction {
    Left,
    Right,
}

struct SpeedTimer {
    tick: u32,
    speed: i32,
}

struct Player {
    state: &'static State,
    timestamp: Option<SystemTime>,
    tick: u32,
    direction: &'static Direction,
    x: i32,
    y: i32,
    run: bool,
    speed_x: i32,
    speed_y: i32,
    speed_x_timers: Vec<SpeedTimer>,
    speed_y_timers: Vec<SpeedTimer>,
    w: u32,
    h: u32,
    falling: bool,
}

struct Wall {
    x1: i32,
    y1: i32,
    x2: i32,
    y2: i32,
}

struct World {
    walls: Vec<Wall>,
}

fn load_textures(canvas: &mut Canvas<Window>, texture_creator: TextureCreator<WindowContext>, sprites: Vec<&'static Sprite>) -> HashMap<&'static str, PlayerTexture> {
    let mut textures = HashMap::with_capacity(sprites.len());

    for path in sprites.iter().map(|x| x.path()) {
        if !textures.contains_key(path) {
            let normal = texture_creator.load_texture(path).unwrap();
            let query = normal.query();
            let mut flipped = texture_creator
                .create_texture_target(texture_creator.default_pixel_format(), query.width, query.height)
                .unwrap();

            canvas.with_texture_canvas(&mut flipped, |texture_canvas| {
                texture_canvas.copy_ex(&normal, None, None, 0.0, None, true, false).unwrap();
            }).unwrap();

            textures.insert(path, PlayerTexture { normal, flipped });
        }
    }
    textures.shrink_to_fit();

    textures
}

macro_rules! elapsed {
    ($system_time:expr) => {{
        $system_time.elapsed().map(|x| x.as_millis()).unwrap_or(0)
    }}
}

macro_rules! update_animation {
    ($state:expr, $value:expr) => {{
        $state.sprite = $value;
        $state.timestamp = Some(SystemTime::now());
        $state.movement_step = 0;
        $state.tick = 0;
    }}
}

macro_rules! update_speed {
    ($timers:expr, $tick:expr) => {{
        let (applied, kept): (Vec<_>, Vec<_>) = $timers.into_iter().partition(|x| x.tick == $tick);
        $timers = kept;
        applied.into_iter().map(|x| x.speed).sum::<i32>()
    }}
}

macro_rules! within_unit {
    ($value:expr, $unit:expr) => {{
        std::cmp::max(std::cmp::min($value, $unit), -$unit)
    }}
}

fn main() {
    sdl2::image::init(sdl2::image::INIT_PNG).unwrap();
    let sdl_context = sdl2::init().unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let video = sdl_context.video().unwrap();
    let mut timer = sdl_context.timer().unwrap();
    let window = video
        .window("Action Platformer", 800, 700)
        .position(0, 0)
        .borderless()
        .build();
    let mut canvas = window.unwrap().into_canvas().build().unwrap();
    let zoom = 4.0;
    canvas.set_scale(zoom, zoom).unwrap();
    let texture_creator = canvas.texture_creator();

    let textures = load_textures(&mut canvas, texture_creator, vec![
        &Idle1, &Idle2, &Idle3, &Idle4,
        &Running1, &Running2, &Running3, &Running4, &Running5, &Running6,
        &PrepareJump1, &PrepareJump2, &PrepareJump3, &PrepareJump4,
        &Jumping1,
        &Falling1, &Falling2,
    ]);

    let unit = 100;
    let run_mult = 2;
    let falling_speed = 40;
    let mut player = Player {
        state: &State::Idle,
        timestamp: None,
        tick: 0,
        direction: &Direction::Right,
        x: 0,
        y: 0,
        run: false,
        speed_x: 0,
        speed_y: 0,
        speed_x_timers: vec![],
        speed_y_timers: vec![],
        w: 10,
        h: 10,
        falling: false,
    };
    let mut animation = Animation {
        sprite: &Idle1,
        textures,
        timestamp: None,
        movement_step: 0,
        tick: 0,
    };
    let world = World {
        walls: vec![
            Wall {
                x1: 0,
                y1: 0,
                x2: 1000 * unit,
                y2: 0,
            },
        ],
    };

    let mut frames = 0;
    let mut now = SystemTime::now();
    let src = Rect::new(0, 0, 50, 37);
    let ground = (700.0 as f32 / zoom - 37.0) as i32;
    let mut dst = Rect::new(0, ground, 50, 37);
    //let mut watcher = vec![];
    'running: loop {

        if player.timestamp.is_none() || elapsed!(player.timestamp.unwrap()) >= 4 {
            let tick = player.tick + 1;

            player.speed_x += update_speed!(player.speed_x_timers, tick);
            player.speed_y += update_speed!(player.speed_y_timers, tick);

            match player.state {
                &State::Idle => {},
                &State::Running => {
                    if player.run {
                        animation.movement_step += 25 * run_mult as u32;
                    } else {
                        animation.movement_step += 25;
                    }
                },
                &State::Jumping => {
                    animation.movement_step += 25;
                },
                &State::PrepareJump => {
                    if animation.tick == 3 {
                        update_animation!(animation, &Jumping1);
                        player.state = &State::Jumping;
                        player.speed_y = unit;
                        player.speed_y_timers.push(SpeedTimer {
                            tick: tick + 60 + 1,
                            speed: -unit,
                        });
                    }
                }
            }

            let player_x1 = player.x / unit;
            let player_x2 = (player.x + player.w as i32) / unit;
            let player_y1 = player.y / unit;
            let player_y2 = (player.y + player.h as i32) / unit;
            let falling = !world.walls
                .iter()
                .any(|wall| (player_x1 >= wall.x1 / unit && player_x2 <= wall.x2 / unit) && player_y1 == wall.y1 / unit);
            if player.state != &State::Jumping {
                if falling && !player.falling {
                    player.speed_y -= falling_speed;
                    player.falling = true;
                    update_animation!(animation, &Falling1);
                } else if !falling && player.falling {
                    player.speed_y += falling_speed;
                    player.falling = false;
                    update_animation!(animation, &Idle1);
                }
            }

            if player.run {
                player.x += within_unit!(player.speed_x * run_mult, unit);
            } else {
                player.x += within_unit!(player.speed_x, unit);
            }
            player.y += within_unit!(player.speed_y, unit);

            player.tick = tick;
            player.timestamp = Some(SystemTime::now());
        }

        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        let texture = &animation.textures
            .get(animation.sprite.path())
            .map(|x| match player.direction {
                &Direction::Right => &x.normal,
                &Direction::Left => &x.flipped,
            })
            .unwrap();
        dst.x = player.x as i32 / unit;
        dst.y = ground - player.y as i32 / unit;
        canvas.copy(texture, src, dst).unwrap();
        canvas.present();
        frames += 1;

        match animation.sprite.duration() {
            &SpriteDuration::Ms(ms) if animation.timestamp.map(|x| elapsed!(x) as u32 >= ms).unwrap_or(true) => {
                animation.sprite = animation.sprite.next();
                animation.timestamp = Some(SystemTime::now());
                animation.tick += 1;
            },
            &SpriteDuration::Step(step) if animation.movement_step / 100 >= step => {
                animation.sprite = animation.sprite.next();
                animation.movement_step = 0;
                animation.tick += 1;
            },
            &SpriteDuration::Infinite => {},
            _ => {},
        }

        let ms_passed = elapsed!(now);
        if ms_passed >= 500 {
            println!("{} fps / x={} y={} animation_tick={}", frames as f32 * 1000.0 / ms_passed as f32, player.x, player.y, animation.tick);
            println!("size: {}", mem::size_of_val(animation.sprite));
            //println!("{:?}", watcher);
            //watcher.clear();
            frames = 0;
            now = SystemTime::now();
        }

        for event in event_pump.poll_iter() {
            match event {
                Event::KeyDown { scancode: Some(Scancode::Escape), .. } => break 'running,
                Event::KeyDown { scancode: Some(Scancode::LShift), .. } if !player.run => {
                    println!("runing");
                    player.run = true;
                },
                Event::KeyUp { scancode: Some(Scancode::LShift), .. } => {
                    println!("not runing");
                    player.run = false;
                },
                Event::KeyUp { scancode: Some(Scancode::Space), .. } => {
                    println!("jumping");
                    update_animation!(animation, &PrepareJump1);
                    player.state = &State::PrepareJump;
                },
                Event::KeyDown { scancode: Some(Scancode::D), .. } => {
                    if player.state == &State::Idle && !player.falling {
                        println!("now running");
                        update_animation!(animation, &Running1);
                    }
                    player.state = &State::Running;
                    player.direction = &Direction::Right;
                    player.speed_x = unit / 4;
                },
                Event::KeyDown { scancode: Some(Scancode::A), .. } => {
                    if player.state == &State::Idle && !player.falling {
                        println!("now running");
                        update_animation!(animation, &Running1);
                    }
                    player.state = &State::Running;
                    player.direction = &Direction::Left;
                    player.speed_x = -unit / 4;
                },
                Event::KeyUp { scancode, .. } if ((scancode == Some(Scancode::A) && player.direction == &Direction::Left) || (scancode == Some(Scancode::D) && player.direction == &Direction::Right)) => {
                    println!("now idle");
                    if player.state == &State::Running {
                        if !player.falling {
                            update_animation!(animation, &Idle1);
                        }
                        player.state = &State::Idle;
                    }
                    player.speed_x = 0;
                },
                _ => {},
            }
        }

        //timer.delay(5);
    }

}
